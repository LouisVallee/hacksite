const express = require("express");
const session = require("express-session");
const app = express();
const path = require("path");
const router = express.Router();
var test=0
/*setTimeout(test=0,1000 * 60 * 3)
function resetValue(){
  return 0
}*/


app.use(express.urlencoded({ extended: true }));
var client = require('redis').createClient()
 
var limiter = require('express-limiter')(app, client)
limiter({
  path: '/login',
  method: 'post',
  lookup: ['connection.ipAddress'],
  // 150 requests per hour
  total: 3,
  expire: 1000 * 60 * 3
})
app.set('trust proxy',1)
app.use(session({
  secret:'s3cur3',
  name:'sessionId'
}))

const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    password : 'Cicyerlo',
    database : 'monsite',
    debug    :  false
});


app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

router.get("/", (req, res) => {
  res.render("index");
});

router.get("/about", (req, res) => {
  res.render("about", { title: "A propos", message: "Page a propos de ce site" });
});

router.get("/login", (req, res) => {
  res.render("login");
});

router.post("/login", (req, res) => {
  //test=test+1
  //enregistrer le user sur une session
  console.log(test)
  if (test>=3){
    res.render("login", {errorMessage:"nombre de tentative dépassé, réessayé plus tard"});
    return
  }else{
  let request= "SELECT * FROM site_user where name='"+req.body.uname+"' and password='"+req.body.psw+"';"
  console.log(request)
  pool.query(request,(err, data) => {
    if(err) {
      //console.error(err);
      res.render("login", {errorMessage:"Erreur interne, reassayer ultérieurement..."});
      return;
    }
    if( data.length > 0){
      //console.log(data)
      //console.log(req.session)
      req.session.user=data;
      res.redirect("/admin");
    }else{
      res.render("login", {errorMessage:"nom d'utilisateur ou mot de pass incorrect"});
    }
  });
  }
});

app.get("/admin", (req, res) => {
  if (req.session.user==null){
    res.redirect("/login")
  } else {
    res.render("admin")
  }
});



app.use("/", router);
app.listen(process.env.port || 3000);

console.log("Running at Port 3000");